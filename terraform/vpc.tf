data "aws_availability_zones" "available" {
  state = "available"
}

locals {
  sorted_availability_zones = sort(data.aws_availability_zones.available.names)
  selected_availability_zones = toset([
    local.sorted_availability_zones[0],
    local.sorted_availability_zones[1],
    local.sorted_availability_zones[2],
  ])
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "~> 5.5.2"

  name                        = "eric-tt-vpc"
  cidr                        = var.aws_vpc_cidr
  azs                         = [local.sorted_availability_zones[0]]
  public_subnets              = [var.aws_vpc_subnet_cidr]
  default_security_group_name = "eric-default-sg"
  enable_nat_gateway          = false

  tags = {
    Terraform = "true"
    Type      = "eric-tt"
  }
}

resource "aws_security_group" "this" {
  name = "rocketchat-sg"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 3000
    to_port     = 3000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = module.vpc.vpc_id

  tags = {
    Name = "rocketchat-sg"
  }
}
