locals {
  server_ami = (var.ami_id != "" ? var.ami_id : data.aws_ami.this.id)
}

data "aws_ami" "this" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_key_pair" "this" {
  key_name   = "eric-tt"
  public_key = file(var.aws_pubkey)
}

resource "aws_instance" "this" {
  ami                         = local.server_ami
  instance_type               = var.aws_instance_type
  associate_public_ip_address = true
  key_name                    = aws_key_pair.this.key_name
  subnet_id                   = module.vpc.public_subnets[0]
  vpc_security_group_ids      = [aws_security_group.this.id]

  root_block_device {
    volume_size = "20"
    volume_type = "gp3"
  }

  tags = {
    Name   = "Rocket.Chat"
    groups = "app"
  }
}
