variable "ami_id" {
  description = "AMI for the Rocket.Chat host"
  type        = string
  default     = ""
}

variable "aws_region" {
  description = "AWS region"
  default     = "us-east-1"
}

variable "aws_pubkey" {
  description = "Public key"
  default     = "../id_rsa.pub"
}

variable "aws_vpc_cidr" {
  description = "AWS VPC CIDR"
  default     = "10.10.0.0/16"
}

variable "aws_vpc_subnet_cidr" {
  description = "AWS VPC subnet CIDR"
  default     = "10.10.10.0/24"
}

variable "aws_instance_type" {
  description = "AWS instance type"
  default     = "t2.small"
}
