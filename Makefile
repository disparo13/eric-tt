AMI := ami-0cf7269b64937fa33

aws-full: AMI :=

all:
	@echo Use \"make prepare\" then: 
	@echo - use \"make aws-full\" or \"make aws-marketplace\"
	@echo - use \"make aws-destroy\" to destroy the configuration.

prepare:
	@test -f ./id_rsa || ssh-keygen -t rsa -N '' -C 'eric_tt' -f ./id_rsa
	@cd terraform && terraform init

aws-marketplace:
	@cd terraform && terraform apply -var="ami_id=$(AMI)" -auto-approve || exit 1

aws-full: aws-marketplace
	@cd ansible && ansible-galaxy collection install community.docker && sleep 90 && \
		ansible-playbook -i `terraform output -state=../terraform/terraform.tfstate -raw server_ip`, main.yml && \
		cd ../terraform && terraform output

aws-destroy:
	@cd terraform && terraform destroy -auto-approve

clean:
	@rm ./id_rsa ./id_rsa.pub || echo ; rm -rf ./terraform/.terraform || echo ; rm ./terraform/terraform.tfstate ./terraform/terraform.tfstate.backup ./terraform/.terraform.lock.hcl || echo
