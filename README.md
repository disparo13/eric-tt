# Rocket.Chat server deployment

## Initial description

Using the automation tools (or not - **it's up to you!**) creates a FULLY provisioned RocketChat running instance in AWS.

## There are 3 methods:

# Method 1 - The Shortcut

This method is good if you need an instance for a while and don't want to setup anything, or run anything on your local workstation.

1. Open your AWS account Console
2. Follow the link https://aws.amazon.com/marketplace/pp/prodview-4ky7yfikdzfqk and proceed with the application subscription - it's free.
3. Using the subscribed application, proceed with the instance creation
4. Please note that your instance selection **might be not less than t2.small with at least - 2Gb of RAM and 10Gb of storage space!** Otherwise, your instance will get stuck due to insufficient memory/storage space!

# Method 2 - The Shortcut with the AMI from The Marketplace with Ubuntu 20.04

This method is good if you need an instance for a while, ready to run the code on your local workstation, but don't want to use/install Ansible on your local workstation. 
Also you'll get your RocketChat much quicker than in the case of Method 3, but your deployment will be a bit less manageable.

1. Clone repository, ensure that you have the AWS API access configured (with `aws configure` - for example)
2. Check that you have installed:
    * Terraform (1.x)
3. Execute `make prepare` to generate ssh keys and init terraform modules
4. Execute `make aws-marketplace` to deploy your RocketChat deployment.
5. Wait until Terraform finishes its work, and in about 5 minutes after that your RocketChat will be available at http://server_ip:3000 (server_ip you can grab from Terraform's output in your Console)

By default - version 6.5.3 of the RocketChat will be deployed. If you aren't happy with it:

6. Follow the articles 1. and 2. from the Method 1
7. Press the yellow "Continue the Configuration" button
8. On the next page in the dropdown list of the "Software version" pick the version of your choice.
8. Note the `Ami Id:` field's value (like ami-02e820bd371be18e0)
9. Edit the `Makefile` in the root directory of this project and in the first line replace the `AMI` variable's value with one that you got from the art. 8
10. Execute command from the art. 4 

# Method 3 - Full deployment to the Ubuntu 22.04 LTS private server

1. Clone repository, ensure that you have the AWS API access configured (with `aws configure` - for example)
2. Check that you have installed:
    * Terraform (1.x)
    * Ansible (>2.13)
3. Execute `make prepare` to generate ssh keys and init terraform modules
4. Execute `make aws-full` to deploy your RocketChat deployment.
5. Wait until Terraform and Ansible finish its work, and in about 5 minutes after that your RocketChat will be available at http://server_ip:3000 (server_ip you can grab from Terraform's output in your Console)

Additional notes: By default, versions RocketChat 6.5.3 and MongoDB 5.0.24 will be deployed. You can change version numbers in `ansible/rocketchat/vars/main.yml` file.

Here I had to hardfix version of MongoDB `mongodb_release: 5.0.24-debian-11-r0` because the image with tag `bitnami/mongodb:5.0` is broken, see https://github.com/bitnami/containers/issues/61882

# Destroying the deployment

Execute `make aws-destroy` to destroy the deployed configuration.

# Cleanup

Execute `make clean` to delete all the files created during script's work.

# Structure

* Makefile - wrapper for all actions
* terraform - directory with the Terraform code to provision infrastracture in the AWS
* ansible - directory with the Ansible code to setup provisioned instances
* ansible/roles/app_preinst - role using to install Docker and it's additional plugins and packages
* ansible/roles/add_security - role using to setup fail2ban to protect SSH service from bruteforce attacks and ufw firewall manager
* ansible/roles/rocketchat - role using to deploy RocketChat itself with the MongoDB server 

```text

├── Makefile
├── README.md
├── ansible
│   ├── ansible.cfg
│   ├── main.yml
│   └── roles
│       ├── add_security
│       ├── app_preinst
│       └── rocketchat
└── terraform
    ├── main.tf
    ├── outputs.tf
    ├── provider.tf
    ├── variables.tf
    └── vpc.tf

```
